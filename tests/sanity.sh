#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0
source /etc/profile.d/modules.sh

module load mpi/openmpi-x86_64

mpitests-IMB-EXT Window Accumulate
let ret=$ret+$?

mpitests-IMB-IO S_Write_Indv S_Read_Indv
let ret=$ret+$?

mpitests-IMB-IO P_IWrite_Indv P_IRead_Indv
let ret=$ret+$?

mpitests-IMB-IO C_Write_Shared C_Read_Shared
let ret=$ret+$?

mpitests-IMB-IO Open_Close
let ret=$ret+$?

mpitests-IMB-RMA All_put_all All_get_all
let ret=$ret+$?

mpitests-osu_hello
let ret=$ret+$?

mpitests-osu_init
let ret=$ret+$?

module unload mpi/openmpi-x86_64

exit $ret
